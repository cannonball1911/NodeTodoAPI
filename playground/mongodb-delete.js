// const MongoClient = require('mongodb').MongoClient
const {MongoClient, ObjectID} = require('mongodb')

const url = `mongodb://localhost:27017`
const dbName = 'TodoApp'

MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
    if (err) {
        return console.log('Unable to connect to MongoDB server')
    }
    console.log('Connected to MongoDB server')

    const db = client.db(dbName)
    
    // deleteMany
    // db.collection('Todos').deleteMany({text: 'Each lunch'}).then((result) => {
    //     console.log(result)
    // })
    
    // deleteOne
    // db.collection('Todos').deleteOne({text: 'Eat lunch'}).then((result) => {
    //     console.log(result)
    // })

    // findOneAndDelete
    // db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
    //     console.log(result)
    // })

    // db.collection('Users').deleteMany({name: 'Kai'})

    db.collection('Users').findOneAndDelete({
        _id: new ObjectID('5b8eda55b3f9c15940b67048')
    }).then((result) => {
        console.log(result)
    })

    // client.close()
})