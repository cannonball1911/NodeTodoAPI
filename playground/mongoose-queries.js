const {ObjectID} = require('mongodb')

const {mongoose} = require('./../server/db/mongoose.js')
const {Todo} = require('./../server/models/todo.js')
const {User} = require('./../server/models/user.js')

// const id = '5b902ab600197f6bd3ac7ecc11'

// if (!ObjectID.isValid(id)) {
//     console.log('ID not valid')
// }

// Todo.find({
//     _id: id
// }).then((todos) => {
//     console.log('Todos', todos)
// })

// Todo.findOne({
//     _id: id
// }).then((todo) => {
//     console.log('Todos', todo)
// })

// Todo.findById(id).then((todo) => {
//     if (!todo) {
//         return console.log('ID not found')
//     }
//     console.log('Todo by ID', todo)
// }).catch((err) => console.log(err))

const userID = '5b900fd642a4bf676d226516'

User.findById(userID).then((user) => {
    if (!user) {
        return console.log(`User with ID '${userID}' not found!`)
    }

    console.log('User by ID: ', JSON.stringify(user, undefined, 2))
}).catch((err) => console.log(err))