const {ObjectID} = require('mongodb')

const {mongoose} = require('./../server/db/mongoose.js')
const {Todo} = require('./../server/models/todo.js')
const {User} = require('./../server/models/user.js')

// Todo.remove({}).then((res) => {
//     console.log(res)
// })

// Todo.findOneAndRemove()

// Todo.findOneAndRemove({
//     _id: '5b918b7b3c19407961721e6f'
// }).then((todo) => {
//     console.log(todo)
// })

Todo.findByIdAndRemove('5b918bd13c19407961721e88').then((res) => {
    console.log(res)
})