# NodeTodoAPI
A Simple Todo API, written in Node.js

## After cloning
- Use `npm install` to install required node modules
- Create a `config.json` file in `server/config/` folder
- Use `npm test` to check if everything works

### config.json example
```json
{
    "test": {
        "PORT": 3000,
        "MONGODB_URI" : "mongodb://localhost:27017/TodoApiTest",
        "JWT_SECRET": "abc123"
    },
    "development": {
        "PORT": 3000,
        "MONGODB_URI": "mongodb://localhost:27017/TodoApi",
        "JWT_SECRET": "123abc"
    }
}
```